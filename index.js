function greetHello(name){
	return 'Hello ${name}';
}

greetHello("Juan");

let sagotNgNililigawanKo = true;

if(sagotNgNililigawanKo){
	console.log("Yehey! Kami na! Hindi na ako kasama sa SMP");
} else {
	console.log("Inuman nalang ng redhorse, kasama nanaman sa SMP");
}

console.log(sagotNgNililigawanKo);


//Loops


let students = ["TJ", "Mia", "Tin", "Chris"];

console.log(students[0]);
console.log(students[1]);
console.log(students[2]);
console.log(students[3]);


// While Loop
let count = 5;

while(count !== 0){
	console.log("Sylvan");
	count--;
}



//  Print numbers 1 to 5 using while
let number = 1;

while(number <= 5 ){
	console.log("number");
	number++;
}


let fruits = ['Banana', 'Mango'];


// furits [0]
// frutis [1]

let indexNumber = 0;

while(indexNumber <= 1){
	console.log(fruits[indexNumber]);
	indexNumber++;
}

let mobilePhones = ['Samsung S21', 'Iphone 13', 'Xiaomi', 'Realme', 'Huawei', 'Pixel 5', 'Asus 6', 'Nokia', 'Cherry mobile'];

console.log(mobilePhones.length);
console.log(mobilePhones.length -1);// will give us the last index position of an element in an array);
console.log(mobilePhones[mobilePhones.length - 1]);


let indexNumberForMobile = 0;

while(indexNumberForMobile <= mobilePhones.length-1){
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}

//Do-while

let countA = 1;

do { //execute the statement
	console.log("Juan");
	countA++; //6+1 = 7	
} while(countA <= 6) //7 <= 6? false




let indexnumberA = 0

let computerBrands = ['Apple Macbook Pro', 'HP NoteBook', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei'];

do {
	console.log(computerBrands[indexnumberA]);
	indexnumberA++;
} while(indexnumberA <= computerBrands/length -1);

//For loop;

for(let count=5; count <= 0; count--){
	console.log(count);
}

let colors = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'White', 'Black'];

for(let i = 0; i <= colors.length - 1; i++){
	console.log(colors[i]);
}

//==================================
//Continue and break
//break - stops the execution of the code
//continue - skip a block code and continue to the next iteration

//ages:
//	18,19, 20, 21, 24, 25
//	age == 21 (debutante age of boys), we will skip then go to the next iteration
//	18, 19, 20, 24, 25

let ages = [18, 19 , 20, 21, 24, 25];
// skip the debutante of boys and girls using continue keyword
// 2<= 5 ? true // 1+1 = 2

for (let i = 0; i <= ages.length - 1; i++){
		//ages[3] -> 21
	if(ages[i] == 21 || ages [i] == 18){
		continue;
	}
	console.log(ages[i]);
}

//=================================


/*
 let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];

 once we found Jayson on our array, we will stop the loop

 Den
 Jayson

 */
let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];
	//1 <= 3? true // 0+1 =1
for (let i = 0; i <=studentNames.length -1; i++){
	//studentNames[1] => Jayson
	if(studentNames[i] == "Jayson"){
		console.log(studentNames[i])
		break;
	}
}

//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/

// Solution:

for (let i = 0; i <= adultAge.length - 1; i++){
	if(adultAge[i] < 20){
		continue;
	}
	console.log(adultAge[i]);
}

//=================================



/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/

// let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];




let student = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(student){
	for (let i = 0; i <=student.length -1; i++){
		if(student[i] == student){
			continue;
		}
		console.log(student);
		break;
	}
}

searchStudent('Jazz');

